# Clustering component

# TODO:s
- Currently multiple similar but technically different types are
  used for storing points in lists.Use the same type whenever 
  possible to reduce complexity.
- Use higher order functions instead of recursion when possible
  for better [insert apprortiate code complexity heuristic here]
- Use ad hoc polymorphism for runLloydOnGraph/runLloyd

## Related sources
- http://www.cs.rit.edu/~rpj/courses/bic2/lectures/kmeansanalysis.pdf
- http://en.wikipedia.org/wiki/Dimension_reduction
- http://programmers.stackexchange.com/questions/184568/turning-n-dim-points-into-m-dim-where-mn-and-where-point-to-point-distance-devi#184568

## Possible problems with lloyd:
* multiple disconnected graphs!?
   > Lloyd should have no problem with disconnected graphs
* graphs with 1 node ("singles")
   > singles should become null vectors.. no prob?
* Need to add ranking of groups and group members (this can probably wait)
   - Establish order of groups and group members by comparing 
     score and possibly lloyd internals
   - Now fixed!
