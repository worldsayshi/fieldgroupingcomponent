package org.apache.solr.handler.component.grouping

import org.apache.solr.handler.component.grouping.UnionFind._

/*
 * Implementation of Kruskals algorithm.
 * Not used at the moment,
 * kept for sentimental reasons.
 */
object Kruskal {
  def runKruskal(nodes : List[Node], edges : List[Edge]) : List[Edge] = {
    // Sort edges in ascending order
    val sortedEdges = edges.sort({_.weight < _.weight})
    val uf = makeUF(nodes)

    return findEdges(sortedEdges, uf, List())
  }
  private def findEdges[E](in_edges : List[Edge], uf : UnionF[E], out_edges : List[Edge]) : List[Edge] =
    if (in_edges.length == 0)
      out_edges
    else {
      val test_edge = in_edges.head
      val m = find(uf, test_edge.from)
      val n = find(uf, test_edge.to)
      val out_edges2 =
        if (m != n)
          test_edge +: out_edges
        else
          out_edges
      findEdges(in_edges.tail, merge(uf, m, n), out_edges2)
    }
}