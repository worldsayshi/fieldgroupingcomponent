package org.apache.solr.handler.component.grouping
import scala.collection.immutable.HashSet
import scala.collection.GenTraversableOnce
import scala.collection.IterableView
import scala.collection.mutable.ArraySeq
import scala.collection.immutable.Nil

object Utils {
  def xor(x : Boolean, y : Boolean) = (x || y) && !(x && y)
  def distance(V1 : Vector[Double], V2 : Vector[Double]) : Double =
    Math.sqrt(V1.zip(V2).foldLeft(0.0) { case (sum, (v1, v2)) => sum + Math.pow(v1 - v2, 2) })
  def sum(V : Seq[Double]) : Double =
    V.foldRight(0.0) { (a : Double, b : Double) => a + b }
  def mean(V : Vector[Double]) : Double =
    (sum(V)) / V.length
  def norm(V : Vector[Double]) : Double =
    Math.sqrt(sum(V.map((v : Double) => v * v)))
}



object UnionFind {
  type UnionF[E] = List[List[E]]
  def makeUF[E](initSet : Collection[E]) : UnionF[E] = {
    return List() ++ (for (elem : E <- initSet)
      yield List(elem))
  }
  def find[E](set : UnionF[E], elem : E) : Int = {
    val l : IterableView[(List[E], Int), Iterable[_]] = set.view.zipWithIndex
    return l.foldLeft(-1) {
      case (j, (subset, i)) =>
        if (j != -1) j
        else (if (subset.contains(elem)) i else -1)
    };
  }
  def merge[E](l : UnionF[E], n : Int, m : Int) : UnionF[E] = {
    if (m == n) return l
    // remove the m:th element
    val l2 = l.slice(0, m) ++ l.slice(m + 1, l.length)
    // and merge it with the n:th
    return l2.updated(n, (l(n) ++ l(m)).distinct)
  }
}

/*
 * Graph data structures
 */

case class Node(val label : Int) {
  override def equals(that : Any) : Boolean =
    (that.isInstanceOf[Node]
      && this.label == that.asInstanceOf[Node].label)
}
case class Edge(val from : Node, val to : Node, val weight : Int) {
  // Check for value equality
  override def equals(that : Any) : Boolean =
    (that.isInstanceOf[Edge]
      && this.weight == that.asInstanceOf[Edge].weight
      // bi/non-directional edges
      && Utils.xor(this.from == that.asInstanceOf[Edge].from
        && this.to == that.asInstanceOf[Edge].to, this.to == that.asInstanceOf[Edge].from
        && this.from == that.asInstanceOf[Edge].to))
}

case class Graph(val nodes : List[Node],val edges : List[Edge])
case class EuclideanGraph(val nodes : List[Node],val edges : List[Edge],val euclidian : Vector[LabeledPoint[Double]])

case class LabeledPoint[T](val label : Int, val pos : Vector[T])
/*
 * Transforming nodes of a graph to an euclidean space
 * based on adjacencies. Each dimension corresponds to
 * a node. (This should be the same as creating a adjacency
 * matrix.)
 * Some inspiration from here:
 * http://www.cs.princeton.edu/courses/archive/spring12/cos598C/SVDforclustering.pdf
 */
object Euclidean {
  def graphToEuclidean(graph : Graph) : EuclideanGraph = {
    val edgeTuples = for (e <- graph.edges)
    yield if (e.from.label < e.to.label) (e.from.label, e.to.label) else (e.to.label, e.from.label)
    val nodeLabels = (for (e <- edgeTuples; n <- List(e._1, e._2)) yield n).distinct
    val nr = nodeLabels.length
    println("Nr of nodes: " + nr)
    val euclidean = goThroughEdges(edgeTuples, nr)
    EuclideanGraph(graph.nodes,graph.edges,euclidean)
  }
  private def goThroughEdges(edgeTuples : List[(Int, Int)], nr : Int) : Vector[LabeledPoint[Double]] = {
    def incVector(v : Vector[Double], j : Int) : Vector[Double] = v.updated(j, v(j) + 1.0)
    println("Nr of edges to go through: " + edgeTuples.length)
    edgeTuples.foldLeft(Vector.fill(nr)(LabeledPoint(-1, Vector.fill(nr)(0.0)))) {
      case (lps, (i, j)) => // Update the i:th node
        val lps2 = lps.updated(i, LabeledPoint(i, incVector(lps(i).pos, j)))
        // update the j:th node
        val lps3 = lps2.updated(j, LabeledPoint(j, incVector(lps2(j).pos, i)))
        lps3
    }
  }
  
  /*
   * Allows arithmetic operations on vectors
   */
  implicit def numVectorify(v : Vector[Double]) = NumVector(v)
  case class NumVector(v1 : Vector[Double]) {
    def -(v2 : Vector[Double]) = {
      v1.zip(v2).map(({ (a : Double, b : Double) => a - b }).tupled(_))
    }
  }
}


  