package org.apache.solr.handler.component.grouping
import org.apache.solr.handler.component.grouping.EuclideanGraph._
import org.apache.solr.handler.component.grouping.Euclidean._
import org.apache.solr.handler.component.grouping.Utils._
import scala.collection.Seq
import scala.util.Random.nextDouble
import collection.breakOut
import scala.util.Random

/*
 * Lloyd clustering algorithm implementation
 */
case class Lloyd(val nrOfClusters : Int, val targetDiff : Double, val clusterSize : Int) {

  type Cluster = List[LloydPoint]
  type LloydPoint = LabeledPoint[Double]
  case class Candidate2(val history : Double, centroids : Seq[Vector[Double]], clusters : List[Cluster])

  // Intended entry point
  def runLloyd(graph : EuclideanGraph, rand : Random) : List[Cluster] = {
    val lloyd_points : List[LloydPoint] = graph.euclidian.toList
    // run the clustering algorithm
    return runLloyd(lloyd_points, rand)
  }
  def runLloyd(points : List[LloydPoint], rand : Random) : List[Cluster] = {
    // No points => empty cluster list
    if (points.length <= 0) {
      return List()
    }
    // Nr of dimensions for the clustering space
    val nrDims = points(0).pos.length
    // Initialize cluster centroids (and empty centroids)
    val (centroids, clusters) = initializeClusters(nrDims, rand).unzip[Vector[Double], List[LloydPoint]]
    val initialCandidate = Candidate2(-1, centroids, clusters)
    // while changing, apply lloyd's algorithm to the clusters contained by the candidate
    val solutionCandidate = lloydRecursive(initialCandidate, points)
    return solutionCandidate.clusters
  }

  def initializeClusters(nrOfDims : Int, rand : Random) : List[(Vector[Double], Cluster)] = {
    // Initialize centroids randomly
    return for (i <- List.range(0, this.nrOfClusters))
      yield ((for (d <- Vector.range(0, nrOfDims)) yield rand.nextDouble(), List.empty[LloydPoint]))
  }

  def lloydRecursive(candidate : Candidate2, points : List[LloydPoint]) : Candidate2 = {

    val candidate1 = emptyClusters(candidate)
    // Assign each node to the cluster with the closest centroid
    val candidate2 = putPointsIntoClusters(candidate1, points)
    // Update the centroid coordinates given the new clusters
    val candidate3 = updateCentroids(candidate2, candidate.clusters.zipWithIndex)

    val performenceMeasure = objective(candidate3)
    val performanceDiff = performenceMeasure - candidate.history

    // If the improvement given by last iteration was low,
    // consider the result good enough
    if (performanceDiff <= this.targetDiff)
      return candidate3
    else {
      val candidate4 = Candidate2(performenceMeasure, candidate3.centroids, candidate3.clusters)
      lloydRecursive(candidate4, points)
    }
  }
  def emptyClusters(candidate : Candidate2) : Candidate2 = {
    val newClusters = for (i <- List.range(0, this.nrOfClusters)) yield List.empty[LloydPoint]
    Candidate2(candidate.history, candidate.centroids, newClusters)
  }

  // objective_function = sum(sum((each_point_of_cluster_i - each_centroid_of_cluster_i)^2))
  def objective(candidate : Candidate2) : Double = {
    assert(candidate.centroids.length == candidate.clusters.length,
      "Clusters and centroids are not equivalent in lenght: " +
        "(" + candidate.centroids.length + "," + candidate.clusters.length + ")")

    def perCentroid(centroid : Vector[Double], cluster : Cluster) = sum(
      cluster.map((point : LloydPoint) =>
        Math.pow(norm(point.pos - centroid), 2))(breakOut))

    sum(candidate.centroids.zip(candidate.clusters).map((perCentroid _).tupled(_)))
  }

  def updateCentroids(candidate : Candidate2, clusters : List[(Cluster, Int)]) : Candidate2 = clusters match {
    case (cluster, i) :: rest => {
      val centroid = candidate.centroids(i)
      val centroid2 : Vector[Double] = cluster.map((p : LloydPoint) => mean(p.pos))(breakOut)
      updateCentroids(Candidate2(candidate.history, candidate.centroids.updated(i, centroid2), candidate.clusters), rest)
    }
    case Nil => candidate
  }

  def findClosestCentroidNotFull(
    candidate : Candidate2,
    point : LloydPoint) : Int = {
    val indexedCentroids = candidate.centroids.toList.zipWithIndex
    indexedCentroids.foldLeft(-1, Double.PositiveInfinity) {
      case ((bestIndex, bestDistance), (centroid, index)) =>
        val newDistance = distance(centroid, point.pos)
        val isCloser = newDistance < bestDistance
        val isNotFull = candidate.clusters(index).length < this.clusterSize
        if (isCloser && isNotFull)
          (index, newDistance) else (bestIndex, bestDistance)
    } match {
      case (-1, _)        => throw new Error("All clusters are full!")
      case (bestIndex, _) => bestIndex
    }
  }

  def putPointsIntoClusters (
    candidate : Candidate2,
    pointsLeft : List[LloydPoint]) : Candidate2 = pointsLeft match {
    case thisPoint :: rest => {
      val closestCentroidInd = findClosestCentroidNotFull(candidate, thisPoint)
      val candidate2 = insertPointIntoCluster(candidate, closestCentroidInd, thisPoint)
      val rest = pointsLeft.tail
      putPointsIntoClusters(candidate2, rest)
    }
    case Nil => candidate
  }

  def insertPointIntoCluster(
    candidate : Candidate2,
    closestCentroid_i : Int,
    point : LloydPoint) : Candidate2 = {
    val points = candidate.clusters(closestCentroid_i)
    val points2 = points :+ point
    val clusters2 = candidate.clusters.updated(closestCentroid_i, points2)
    val candidate2 = Candidate2(candidate.history, candidate.centroids, clusters2)
    return candidate2
  }
}