package org.apache.solr.handler.component.grouping

import java._
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.asScalaSet
import scala.collection.JavaConversions.mutableMapAsJavaMap
import scala.collection.Seq
import scala.collection.Set
import scala.collection.mutable
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.LinkedList
import org.apache.lucene.document.Document
import org.apache.solr.core.SolrCore
import org.apache.solr.core.SolrEventListener
import org.apache.solr.handler.component.ResponseBuilder
import org.apache.solr.handler.component.SearchComponent
import org.apache.solr.search.SolrIndexSearcher
import org.apache.solr.util.plugin.SolrCoreAware
import scala.collection.mutable.Buffer
import scala.collection.mutable.ListBuffer
import com.sun.org.apache.xalan.internal.xsltc.compiler.ForEach
import org.apache.solr.common.util.NamedList
import collection.breakOut
import scala.collection.immutable.SortedMap
import scala.collection.immutable.TreeMap
import org.apache.solr.handler.component.grouping.Euclidean._
import org.apache.solr.response.SolrQueryResponse
import scala.util.Random

trait CoreEventListener extends SolrCoreAware with SolrEventListener {};

class InputGroup(var fields : Seq[String], var score : Float)

/*
 * The FieldGroupingComponent is the interface supplied to
 * Solr. It mainly serves as a wrapper for the Lloyd module.
 * It gathers the necessary object for the grouping algorithm
 * to work and appropriates the field name so that lloyd can 
 * work with it. 
 *   Here is also a home brewn algorithm taken out of service.
 * It can be switched to by the 'groupingScheme' constant.
 */
class FieldGroupingComponent extends SearchComponent with CoreEventListener {

  var inputGroups = new LinkedList[InputGroup]
  var inpArgs : NamedList[_] = null
  var maxGroupSize : Int = 6
  var ignoreList = new HashSet[String]

  // groupingScheme = homebrewn | lloyd
  val groupingScheme = "lloyd"

  override def init(args : NamedList[_]) = {
    inpArgs = args
    // debug assertions (it should be ok for them to be null):
    assert(inpArgs.get("maxGroupSize") != null)
    assert(inpArgs.get("ignoreList") != null)
    inpArgs
      .get("ignoreList")
      .toString
      .replaceAll("\\s", "")
      .split(",")
      .foreach(ignoreProp => ignoreList += ignoreProp)
    maxGroupSize = Integer.parseInt(inpArgs.get("maxGroupSize").toString)
  }

  // process is called from Solr at the end of a search request
  // ResponseBuilder carries data sent back as response to the query
  def process(rb : ResponseBuilder) = {

    assert(rb.getResults().docList.hasScores(), 
        "FieldGroupingComponent needs scored documents!")
    var rs = rb.getResults().docList.iterator()
    var allFieldNames = new HashSet[String]
    inputGroups = new LinkedList[InputGroup]

    // Turn documents into input field groups
    while (rs != null && rs.hasNext()) {
      var docId : Int = rs.nextDoc()
      var doc : Document = searcher.doc(docId)
      var fieldNames = new ListBuffer[String]
      for (field <- doc.getFields()) if (!ignoreList.contains(field.name)) fieldNames += field.name
      var score = rs.score
      allFieldNames ++= fieldNames
      inputGroups = inputGroups append LinkedList(new InputGroup(fieldNames, score))
    }

    var adjacencies = createAdjacencyTable(inputGroups)
    var singles = gatherSingles(allFieldNames, adjacencies)
    var unorderedOutputGroups : Seq[Seq[String]] 
    	= applyGroupingScheme(adjacencies, inputGroups, singles, allFieldNames)

    var orderedOuputGroups = orderByPropFreq(rb.rsp,unorderedOutputGroups)
    rb.rsp.add("fieldgroups", submissionFormat(orderedOuputGroups))

  }

  def orderByPropFreq(rsp : SolrQueryResponse, groups : Seq[Seq[String]]) 
  : Seq[Seq[String]] = try {
    // facet_counts.facet_fields.propertyNames
    print("[FieldGrouping] orderByPropFreq.facet_counts.facet_fields.propertyNames:  ")
    val fc = rsp.getValues().get("facet_counts").asInstanceOf[NamedList[NamedList[_]]]
    val pn = fc.get("facet_fields").get("propertyNames").asInstanceOf[NamedList[Int]]
    val annotGroups = groups map { group =>
      val annotGroup = group map { prop =>
        (pn.get(prop),prop)
      }
      val sumFreq = annotGroup.foldLeft(0){_ + _._1}
      val freqScore = if(annotGroup.length<=0) 0 
    		  	else sumFreq / annotGroup.length
      val sortedAnnotGroup = annotGroup.sortBy(-_._1)
      val sortedGroup = sortedAnnotGroup map (_._2)
      (freqScore,sortedGroup)
    } 
    val sortedAnnotGroup = annotGroups.sortBy(-_._1)
    val sortedGroups = sortedAnnotGroup map (_._2)
    sortedGroups
  } catch {
    case e : NullPointerException => {
      print("Field frequency was not attached! Field ordering skipped!")
      groups
    }
  }

  def submissionFormat(groups : Seq[Seq[String]]) = {
    var groupsFormat = new Array[NamedList[String]](groups.length)
    var i = 0
    for (g <- groups) {
      var groupF = new NamedList[String]
      for (f <- g) groupF.add(f, null)
      groupsFormat.update(i, groupF)
      i += 1
    }
    groupsFormat
  }

  type AdjEntry = (Int, mutable.HashMap[String, Int])
  class AdjacencyTable extends HashMap[String, AdjEntry]

  def createAdjacencyTable(inputGroups : Seq[InputGroup]) : AdjacencyTable = {
    var l = for (g1 <- inputGroups) yield for (
      f1 <- g1.fields;
      f2 <- g1.fields if !f1.equals(f2)
    ) yield (f1, f2)
    var l2 = l.flatten
    var table = new AdjacencyTable
    l2 foreach {
      case (f1, f2) => incrementTablevalues(table, f1, f2)
      // Impossible: (No not if the list is empty right?)
      case p        => throw new Exception("Pair did not match pattern: " + p)
    };
    //table.checkCorrectOrder
    table
  }

  def gatherSingles(fields : Set[String], adjacencies : AdjacencyTable) = for (f <- fields if !adjacencies.contains(f)) yield f

  def incrementTablevalues(table : AdjacencyTable, f1 : String, f2 : String) = {
    def incrAdj(adj : mutable.HashMap[String, Int]) = adj += f2 -> (if (adj.contains(f2)) (adj(f2) + 1) else 1)

    table += f1 -> (table.getOrElse(f1, (1, new mutable.HashMap[String, Int])) match {
      case (connectivity, adj) => (connectivity + 1, incrAdj(adj))
    })
  }

  def applyGroupingScheme(
    adjacencies : AdjacencyTable,
    inputGroups : LinkedList[InputGroup],
    singles : Set[String],
    allFieldNames : Set[String]) : Seq[Seq[String]] = {
    this.groupingScheme match {
      case "homebrewn" => applyHomeMadeGroupingScheme(
        adjacencies : AdjacencyTable,
        inputGroups : LinkedList[InputGroup],
        singles : Set[String],
        allFieldNames : Set[String])
      case "lloyd" => applyLloyd(
        adjacencies : AdjacencyTable,
        inputGroups : LinkedList[InputGroup],
        singles : Set[String],
        allFieldNames : Set[String])
    }
  }

  // The old grouping algorithm
  def applyHomeMadeGroupingScheme(
    adjacencies : AdjacencyTable,
    inputGroups : LinkedList[InputGroup],
    singles : Set[String],
    allFieldNames : Set[String]) : Seq[Seq[String]] =
    {
      var outputGroups = new ListBuffer[ListBuffer[String]]

      var treatedFields = new HashSet[String]
      var remainingNonSingles = sortAdjTableByValue(adjacencies)
      def nextNonSingle() : Entry = {
        while (treatedFields.contains(remainingNonSingles.head.getKey())) {
          remainingNonSingles = remainingNonSingles.tail
          if (remainingNonSingles.length == 0) return null
        }
        val res = remainingNonSingles.head
        remainingNonSingles = remainingNonSingles.tail
        res
      }
      while (remainingNonSingles.length > 0) {
        var entry = nextNonSingle()
        if (entry != null) {
          assert(!treatedFields.contains(entry.getKey()))
          treatedFields += entry.getKey()
          var currentOutputGroup = ListBuffer(entry.getKey())
          var remainingNeighbours = sortNeighbours(entry.getValue()._2)
          def nextNeighbour() : java.util.Map.Entry[String, Int] = {
            while (treatedFields.contains(remainingNeighbours.head.getKey())) {
              remainingNeighbours = remainingNeighbours.tail
              if (remainingNeighbours.length == 0) return null
            }
            val res = remainingNeighbours.head
            remainingNeighbours = remainingNeighbours.tail
            res
          }
          while (remainingNeighbours.length > 0
            && currentOutputGroup.length <= maxGroupSize) {
            var entryN = nextNeighbour()
            if (entryN != null) {
              assert(!treatedFields.contains(entryN.getKey()))
              treatedFields += entryN.getKey()
              currentOutputGroup += entryN.getKey()
            }
          }
          outputGroups += currentOutputGroup
        }
      }
      var singleGroup = new ListBuffer[String]
      singles.foreach(single => {
        assert(!treatedFields.contains(single))
        treatedFields += single
        singleGroup += single
      })
      outputGroups += singleGroup
      assertAllFieldNamesTreated(allFieldNames, outputGroups)
      outputGroups
    }

  def applyLloyd(
    adjacencies : AdjacencyTable,
    inputGroups : LinkedList[InputGroup],
    singles : Set[String],
    allFieldNames : Set[String]) : Seq[Seq[String]] = {
    
    // type changes and creation of various lookup tables
    val allFieldNames2 : List[String] = allFieldNames.toList
    val indexToNameTable = new TreeMap[Int,String] ++ (allFieldNames2.zipWithIndex.map(_.swap))
    val nameToNodeTable : TreeMap[String,Node] = indexToNameTable.map({case (i,name) => (name,Node(i))})
    
    // Create the graph
    val nodes : List[Node] = indexToNameTable.map({case (i,name) => Node(i)})(breakOut)
    val edges = adjacencyTableToEdgeList(adjacencies,nameToNodeTable)
    val graphRepresentation = Graph(nodes : List[Node],edges : List[Edge])
    // And an euclidean-space representation of the graph
    val graphEuclid = graphToEuclidean(graphRepresentation)
    
    val nrOfClusters = allFieldNames2.length/maxGroupSize
    // Initialize lloyd algorithm context
    val lloyd = Lloyd(nrOfClusters+1,0.1,maxGroupSize)
    
    // always using the same seed to guarantee determinism
    val seed : Long = 4983461779405031285L
    // run the lloyd algorithm
    val lloydClusters = lloyd.runLloyd(graphEuclid,new Random(seed))
    
    // convert the clusters to groups of names and return them
    return for(cluster <- lloydClusters) yield
      for(lloydpoint <- cluster; label=lloydpoint.label) 
        yield indexToNameTable.get(label).get
  }
  
  def adjacencyTableToEdgeList(
      adjacencies : AdjacencyTable,
      nameToNodeTable:TreeMap[String,Node]) 
  	: List[Edge] 
  	= {
    // Turn adjacency table to 3-tuples: (name,name,weight)
    val raw_edges : Iterable[(String,String,Int)] 
		= adjacencies.map({
		  case (n1,t2)=>
		    t2._2.map({
		      case (n2,weight)=>
		        (n1,n2,weight)
		    })
		  }).flatten
    // Turn 3-tuples into type Edge
    val edges = raw_edges map {case (name1,name2,w)=>
      Edge(nameToNodeTable.get(name1).get,nameToNodeTable.get(name2).get,w)}
    return edges.toList
  }

  // Some mixed assertions
  // These should be test cases
  // Currently they run on "real life" cases
  // It is only used for the home brewn grouping scheme
  def assertAllFieldNamesTreated(
      allFieldNames : Set[String],
      outputGroups : ListBuffer[ListBuffer[String]]) 
  	= {
    var l = outputGroups.flatten
    var msg = ""
    var pass : Boolean = true
    if (l.length != allFieldNames.size) {
      pass = false
      msg += "Not same size: " + allFieldNames.size + ", " + l.length
    }
    var fieldsCheck = new HashSet[String]
    for (f <- l) {
      if (fieldsCheck.contains(f)) {
        pass = false
        msg += " | " + f + " is duplicated" + " | "
      } else fieldsCheck += f
    }
    var intruders = fieldsCheck -- allFieldNames
    var leftovers = allFieldNames -- fieldsCheck
    if (intruders.size > 0 || leftovers.size > 0) pass = false
    msg += " \nFields not part of allfields(" + intruders.size + "): " + (for (i <- intruders) yield i)
    msg += " \nFields not part of outputGroups(" + leftovers.size + "): " + (for (l <- leftovers) yield l)
    assert(pass, msg)
  }

  def sortNeighbours(neighbours : mutable.HashMap[String, Int]) = {
    val eSet = neighbours.entrySet()
    var l = eSet.toSeq.sortWith(
      {_.getValue() < _.getValue()})
    l = l.reverse
    assert(l.head.getValue() >= l.last.getValue(), "Order should be descending: "
      + (for (e <- l) yield e.getValue()).toString)
    l
  }

  type Entry = java.util.Map.Entry[String, (Int, scala.collection.mutable.HashMap[String, Int])]
  def sortAdjTableByValue(table : AdjacencyTable) = {
    val eSet : Set[Entry] = table.entrySet()
    val l = eSet.toSeq.sortWith(
      (e1, e2) => e1.getValue()._1 > e2.getValue()._1)
    assert(l.head.getValue()._1 >= l.last.getValue()._1)
    l
  }

  /*
   * Collect the searcher 
   * (for looking up documents)
   */
  var searcher : SolrIndexSearcher = null
  def inform(core : SolrCore) = {
    core.registerFirstSearcherListener(this)
    core.registerNewSearcherListener(this)
  }
  def newSearcher(
    newSearcher : SolrIndexSearcher,
    currentSearcher : SolrIndexSearcher) = { this.searcher = newSearcher }

  // Unused interface implementations
  def prepare(rb : ResponseBuilder) = ()
  def postCommit() = ()
  def getSource() = null
  def getVersion() = null
  def getSourceId() = null
  def getDescription() = null

}