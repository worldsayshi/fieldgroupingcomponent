package org.apache.solr.handler.component.grouping
import scala.collection.mutable.Stack
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers._
import org.scalatest.FunSuite
import scala.collection.mutable.Stack
import org.apache.solr.handler.component.grouping.UnionFind._
import org.apache.solr.handler.component.grouping.Kruskal._
import org.apache.solr.handler.component.grouping.Euclidean._
import org.apache.solr.handler.component.grouping.Lloyd
import scala.util.Random

//import scala.util.parsing.json

class Test extends FlatSpec {

  behavior of "Union find"
  val l = List(1, 2, 3)
  assert(l.size == 3)
  val uf = makeUF(l)
  it should "initially have as many elements as added" in {
    assert(uf.size == 3)
  }
  it should "not be affected by merging identical ideces" in {
    assert(merge(uf, 0, 0).size == 3)
    assert(merge(uf, 1, 1).size == 3)
    assert(merge(uf, 2, 2).size == 3)
  }
  it should "become smaller when merging" in {
    assert(merge(uf, 0, 1).size == 2)
    assert(merge(uf, 1, 2).size == 2)
    assert(merge(uf, 0, 1).size == 2)
    assert(merge(uf, 1, 2).size == 2)
    assert(merge(merge(uf, 1, 2), 0, 1).size == 1)

    assert(merge(merge(uf, 1, 2), 0, 1)(0).size == 3)
    assert(merge(merge(uf, 1, 2), 0, 1)(0) == l)
  }
  it should "contain all elements after merge" in {
    merge(uf, 1, 2).flatten.length should equal (uf.length)
  }
  it should "find the correct index of elements" in {
    find(uf, 1) should equal(0)
    find(uf, 2) should equal(1)
    find(uf, 3) should equal(2)
    
    val partiallyMerged = merge(uf, 1, 2)
    find(partiallyMerged,1) should equal (0)
    find(partiallyMerged,2) should equal (1)
    find(partiallyMerged,3) should equal (1)
    
    val allMerged = merge(merge(uf, 1, 2), 0, 1) 
    find(allMerged,1) should equal (0)
    find(allMerged,2) should equal (0)
    find(allMerged,3) should equal (0)
  }

  
  behavior of "Kruskal's algorithm spanning tree"
  
  val ns = for (i <- List.range(0, 4) ) yield new Node(i)
  assert(ns.length==4);
  val edges = List(
		 new Edge(ns(0),ns(1),1)
		,new Edge(ns(0),ns(3),2)
		,new Edge(ns(1),ns(2),2)
		,new Edge(ns(1),ns(2),1)
		,new Edge(ns(2),ns(3),5))
  val graph = Graph(ns,edges)
  
  val minSpanningTree = runKruskal(ns,edges)
		
  it should "include all nodes" in {
    val spanningTreeNodes : List[Node] = (for(e<-minSpanningTree; n <- List(e.to,e.from)) yield n).distinct
    spanningTreeNodes.length should equal (ns.length)
    for(n<-ns)
      spanningTreeNodes should contain (n)
  }
  it should "have no duplicate edges" in {
    minSpanningTree.distinct.length == minSpanningTree.length
  }
  it should "be the correct solution" in {
    var solution = List(edges(0),edges(1),edges(3))
    solution.length should equal (minSpanningTree.length)
    for(e<-solution)
      minSpanningTree should contain (e)
  }
  
  behavior of "Euclidean convertion"
  
  it should "not throw exception" in {
    println("Prouduced vector: "+graphToEuclidean(graph))
  }
  
  behavior of "Lloyd's algorithm"
  
  it should "not throw exception" in {
    val lloyd = Lloyd(2,0.1,6)
    // always using the same seed to guarantee determinism
    val seed : Long = 4983461779405031285L
    println(lloyd.runLloyd(graphToEuclidean(graph),new Random(seed)))
  }

}